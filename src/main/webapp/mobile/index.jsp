<%@ page contentType="text/html; charset=UTF-8" language="java"
	errorPage=""%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Titan Monitor - Openstack monitoring</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script src="jquery-1.10.2.min.js"></script>
	<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.0.min.css" />
	<script src="jquerymobile/jquery.mobile-1.4.0.min.js"></script>
</head>
<body>
	<div data-role="page">
		<div data-role="header">
			<h1>Titan Monitor</h1>
		</div>
		<div data-role="navbar" data-grid="d">
			<ul>
				<li><a href="#" class="ui-btn-active">Stat</a></li>
				<li><a href="#">VM</a></li>
				<li><a href="#">Setting</a></li>
				<li><a href="#">Logout</a></li>
			</ul>
		</div>
		<div role="main" class="ui-content">
			<p>Page content goes here.</p>
		</div>
	</div>
</body>
</html>
