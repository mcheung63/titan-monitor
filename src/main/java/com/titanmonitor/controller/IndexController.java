package com.titanmonitor.controller;

import java.io.File;
import java.io.FileInputStream;
import java.security.Principal;
import java.util.Hashtable;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.titanmonitor.TitanMonitorCommonLib;

@Controller
public class IndexController {
	private static Logger logger = Logger.getLogger(IndexController.class);

	@RequestMapping(value = "/index.htm", method = RequestMethod.GET)
	public String index(ModelMap model) {
		return "index";
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public String login(HttpServletRequest request, Model model, Principal principal, @RequestParam String username, @RequestParam String password) {
		try {
			Properties prop = new Properties();
			FileInputStream inputStream = new FileInputStream(new File(getClass().getResource("/main.properties").toURI()));
			prop.load(inputStream);
			Hashtable<String, String> ht = new Hashtable<String, String>();
			ht.put("username", username);
			ht.put("password", password);
			String result = TitanMonitorCommonLib.sendPost(prop.getProperty("titanRestServer") + "/login.htm", ht);
			JSONObject json = JSONObject.fromObject(result);
			JSONObject arr = json.getJSONObject("values");
			String r = arr.getString("loginResult");
			if (r != null && r.equals("ok")) {
				return "redirect:mobile/index.htm";
			} else {
				model.addAttribute("errorMsg", "Login fail");
				return "index";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "index";
		}
	}
}
