package com.titanmonitor.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MobileController {
	private static Logger logger = Logger.getLogger(MobileController.class);

	@RequestMapping(value = "/mobile/index.htm", method = RequestMethod.GET)
	public String index(ModelMap model) {
		return "mobile/index";
	}

}
